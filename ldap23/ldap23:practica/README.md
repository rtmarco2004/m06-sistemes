# LDAP PRACTICA


## Executar container:

```
docker run --rm --name ldap -h ldap.edt.org --net 2hisx -p 389:389 -it marco3008/ldap23:practica /bin/bash
```


## Crear un schema que contingui:

### Un objecte STRUCTURAL

```
objectClass (1.1.2.1.1.1 NAME 'x-verdura'
  DESC 'Verdura'
  SUP TOP
  STRUCTURAL
  MUST ( x-nom $ x-tipus )
  MAY ( x-imatge $ x-documentacio ))
```

### Un objecte AUXILIARY

```
objectClass (1.1.2.2.1.1 NAME 'x-origen'
  DESC 'Origen de la verdura'
  SUP TOP
  AUXILIARY
  MUST ( x-madura $ x-lloc-creixement $ x-procedencia ))
```

## Atributs del objecte STRUCTURAL

**Definir el nom de la verdura**

```
attributetype (1.1.2.1.1.1 NAME 'x-nom'
  DESC 'Nom de la verdura'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)
```

**A quin tipus pertany**

```
attributetype (1.1.2.1.1.2 NAME 'x-tipus'
  DESC 'A quin tipus pertany'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)
```

**Informacio sobre la verdura**

```
attributetype (1.1.2.1.1.4 NAME 'x-documentacio'
  DESC 'Documentació de la verdura'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.5)
```

**Imatge de la verdura**

```
attributetype (1.1.2.1.1.3 NAME 'x-imatge'
  DESC 'Imatge de la verdura'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28)
```

## Atributs del objecte AUXILIARY

**Maduresa**

```
attributetype (1.1.2.2.1.1 NAME 'x-madura'
  DESC 'Indica si la verdura està madura'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE)
```

**Lloc on creix**

```
attributetype (1.1.2.2.1.2 NAME 'x-lloc-creixement'
  DESC 'Lloc de creixement de la verdura'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)
```

**D'on prove**

```
attributetype (1.1.2.2.1.3 NAME 'x-procedencia'
  DESC 'Procedència de la verdura'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)
```

## Crear 3 entitats

### 1a Entitat:

```
dn: x-nom=patata,ou=practica,dc=edt,dc=org
objectClass: x-verdura
objectClass: x-origen
x-nom: patata
x-tipus: hortalissa
x-imatge:< file:/opt/docker/patata.jpeg
x-documentacio:< file:/opt/docker/patata.pdf
x-madura: True
x-lloc-creixement: terra
x-procedencia: SudAmerica
```

### 2a Entitat:

```
dn: x-nom=pastanaga,ou=practica,dc=edt,dc=org
objectClass: x-verdura
objectClass: x-origen
x-nom: pastanaga
x-tipus: hortalissa
x-imatge:< file:/opt/docker/pastanaga.jpeg
x-documentacio:< file:/opt/docker/pastanaga.pdf
x-madura: False
x-lloc-creixement: terra
x-procedencia: Asia
```

### 3a Entitat

```
dn: x-nom=bleda,ou=practica,dc=edt,dc=org
objectClass: x-verdura
objectClass: x-origen
x-nom: bleda
x-tipus: verdura
x-imatge:< file:/opt/docker/bleda.jpeg
x-documentacio:< file:/opt/docker/bleda.pdf
x-madura: True
x-lloc-creixement: terra
x-procedencia: SudAmerica
```

## Construir l'imatge

```
docker build -t marco3008/ldap23:practica .
```

## Encendre el container

```
docker run --rm --name ldap -h ldap.edt.org --net 2hisx -p 389:389 -it marco3008/ldap23:practica /bin/bash
```

## Encendre el phpldapadmin

```
docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org -p 80:80 --net 2hisx -d marco3008/phpadmin
```

## Encendre amb docker compose 

```
docker compose -f docker-compose.yml up -d
```

## Entrar desde internet a phpldapadmin

```
localhost/phpldapadmin/
```
