# LDAP GRUPS


## Crear una nova ou grups:

```
dn: ou=grups,dc=edt,dc=org
ou: grups
description: Container per usuaris del grup linux
objectclass: organizationalunit
```

## Afegir un gidNumber corresponent a cada usuari:

```
dn: uid=pau,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: Profes
uid: pau
uidNumber: 5000
gidNumber: 601
homeDirectory: /tmp/home/pau
userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/
```

```
dn: uid=admin,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 27
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3
```

```
dn: uid=user01,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user01
cn: alumne01 de 1asix de todos los santos
sn: alumne01
homephone: 555-222-0001
mail: user01@edt.org
description: alumne de 1asix
ou: 1asix
uid: user01
uidNumber: 7001
gidNumber: 610
homeDirectory: /tmp/home/1asix/user01
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=
```

## GuidNumber:

 ● professors /601
 ● alumnes / 600
 ● 1asix / 610
 ● 2asix / 611
 ● sudo / 27
 ● 1wiam / 612
 ● 2wiam / 613
 ● 1hiaw / 614

## Crear els grups nous:

### Professors:

```
dn: cn=1asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1asix
gidNumber: 610
description: grup 1asix
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05
```

### Alumnes:

```
dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: alumnes
gidNumber: 600
description: El grup dels alumnes facheros
memberUid: anna
memberUid: marta
memberUid: jordi
```

### 1Asix:

```
dn: cn=1asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1asix
gidNumber: 610
description: grup 1asix
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05
```

### 2Asix:

```
dn: cn=2asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2asix
gidNumber: 611
description: grup 2asix
memberUid: user06
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10
```

### sudo:

```
dn: cn=sudo,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: wheel
gidNumber: 27
description: Grup de sudo
memberUid: admin
```

### 1Wiam:

```
dn: cn=1wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1wiam
gidNumber: 612
description: cicle extrany
```

### 2Wiam:

```
dn: cn=2wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2wiam
gidNumber: 613
description: 2 any de ni idea 
```

### 1Hiaw:

```
dn: cn=1hiaw,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1hiaw
gidNumber: 614
description: cicle guay
```

### Crear la imatge:

```
docker build -t marco3008/ldap23:grups .
```

### Encendre el docker compose:

```
docker compose -f docker-compose.yml up -d
```

### Entrar desde internet a phpldapadmin:

```
localhost/phpldapadmin
```
