# LDAP EDITAT


## Executar container:

```
docker build -t marco3008/ldap23:editat .
```

```
docker run --rm --name ldap -p 389:389 -d marco3008/ldap23:editat
```

## PAS 1:

### Crear Dockerfile:

```
# ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="@marco3008"
LABEL subject="ldapserver 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```

## PAS 2:

### Crear el fitxer startup.sh

```
#!/bin/bash

echo "Configurant el servidor ldap..."

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
slapcat
/usr/sbin/slapd -d0
```

## PAS 3:

### Fitxers:

Copiem el fitxer slapd.conf i edt-org.ldif, dins del fitxer edt-org.ldif afegim usuaris amb format uid, una nova OU i 2 usuaris nous.

#### Usuaris nous:

```
dn: uid=user01,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user01
cn: alumne01 de 1asix de todos los santos
sn: alumne01
homephone: 555-222-0001
mail: user01@edt.org
description: alumne de 1asix
ou: 1asix
uid: user01
uidNumber: 7001
gidNumber: 610
homeDirectory: /tmp/home/1asix/user01
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=

dn: uid=user02,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user02
cn: alumne02 Pou Prat
sn: alumne02
homephone: 555-222-0002
mail: user02@edt.org
description: alumne de 1asix
ou: 1asix
uid: user02
uidNumber: 7002
gidNumber: 610
homeDirectory: /tmp/home/1asix/user02
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=
```

#### OU nova:

```
dn: ou=cfgs,dc=edt,dc=org
ou: cfgs
description: cicle informatica grau superior
objectclass: organizationalunit
```

#### 2 usuaris nous:

```
dn: cn=grup1,ou=cfgs,dc=edt,dc=org
objectclass: inetOrgPerson
cn: grup1
sn: grup1
description: som el grup 1

dn: cn=grup2,ou=cfgs,dc=edt,dc=org
objectclass: inetOrgPerson
cn: grup2
sn: grup2
description: som el grup 2
```

## PAS 4:

### Encriptar la password:

```
slappasswd -s secret
```

```
#contra secret

rootpw {SSHA}DIKADJbO1Wy4e1htZw7XTbiKsd1ofpNc
```
Esborrem la password secret i posem la encriptada dins del fitxer slapd.conf


## Fitxer per canviar la password en calent amb un fitxer:

```
vim mod1.ldif
```

```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: patata
-
delete: olcAccess
-
add: olcAccess
olcAccess: to * by * write
```

### Comprovació:

```
ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f mod1.ldif
```

```
ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcRootPW olcAccess
```

## Fitxer per modicar nom de un usuari amb un fitxer:

```
mod2.ldif
```

```
dn: uid=user06,ou=usuaris,dc=edt,dc=org
changetype: modify
replace: cn
cn: Manuel
```

### Comprovacio:

```
ldapmodify -vx -D 'cn=Manager,dc=edt,dc=org' -w patata -f mod2.ldif
```

```
ldapsearch -x -LLL -b 'uid=user05,ou=usuaris,dc=edt,dc=org'
```


```
docker cp ldap:/opt/docker/data-futbolA.ldif .
```
