# /usr/bin/python3
#-*- coding: utf-8-*-
# 26-telnet-server-one2one.py  
# -------------------------------------
# @marco3008
# Maig 2024
# -------------------------------------

import sys
import socket
import argparse
import signal
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="telnet server")
parser.add_argument("-p", "--port", type=int,
                    metavar="port",
                    dest="port",
                    default=50001)
parser.add_argument("-d", "--debug", action='store_true')
args = parser.parse_args()

HOST = ''
PORT = args.port
TERMINAR = bytes(chr(4), 'utf-8')
llistaPeers = []
DEBUG = args.debug

# -------------------------------
def llista(signum, frame):
    print("Signal handler called with signal: ", signum)
    print(llistaPeers)
    sys.exit(0)

def contador(signum, frame):
    print("Signal handler called with signal: ", signum)
    print(len(llistaPeers))
    sys.exit(0)

def completo(signum, frame):
    print("Signal handler called with signal: ", signum)
    print(llistaPeers, len(llistaPeers))
    sys.exit(0)

signal.signal(signal.SIGUSR1, llista)  # 10
signal.signal(signal.SIGUSR2, contador)  # 12
signal.signal(signal.SIGTERM, completo)  # 15



s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

while True:
    conn, addr = s.accept()
    print("Connexió per: ", addr)
    llistaPeers.append(addr)
    while True:
        ordre = conn.recv(1024).decode('utf-8').strip()
        if not command:
            break
        if ordre == 'processos':
            command = 'ps ax'

        elif ordre == 'ports':
            command = 'ss -pta'

        elif ordre == 'whoareyou':
            command = 'uname -a'

        else:
            command = 'uname -a' 
        
        pipeData = Popen(command, stdout=PIPE, stderr=PIPE, shell=True)
        for line in pipeData.stdout:
            conn.sendall(line)
        conn.send(TERMINAR)
    conn.close()

