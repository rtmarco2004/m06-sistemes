# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql.py -c numcli [-c numcli...]
#2102
#2111
#2103

import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description='Consulta SQL interactiva de clients')
parser.add_argument('-c',"--client", help='numero client',type=str,\
     action="append",dest="llistaclie",required="True")
args = parser.parse_args()
#print(args)
#exit(0)
# -------------------------------------------------------
command = "PGPASSWORD=passwd  psql -qtA -F',' -h localhost  -U postgres training"
pipeData = Popen(command, shell=True, bufsize=0, \
             universal_newlines=True,
        	 stdin=PIPE, stdout=PIPE, stderr=PIPE)
for num_clie in args.llistaclie:
  sqlConsulta="select * from clientes where num_clie=%s;" % (num_clie)
  pipeData.stdin.write(sqlConsulta+"\n")
  print(pipeData.stdout.readline(), end="")
pipeData.stdin.write("\q\n")
exit(0)

