#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n 5 o 10 o 15] [-f file]...
# default=10
# @marco3008 M06 Curs 2023-2024
#--------------------

import sys
import argparse

parser = argparse.ArgumentParser(\
    description="mostra les n linies del argument",\
    prog="03-arguments.py",\
    epilog="Esto es todo esto todo amigos :)")

parser.add_argument("-n","--nlin",dest="nlin", default=5, type=int,\
    help="numero de linies a mostrar", metavar="NumeroLinies" , choices=[5,10,15])

parser.add_argument("fileList",\
    help="fitxer a processar",\
    metavar="file", nargs="*")

parser.add_argument("-v", "--verbose", action="store_true")

args=parser.parse_args()
print(args)


MAX=args.nlin

def headFile(fitxer):
    fileIn=open(fitxer,"r")
    counter=0
    
    for line in fileIn:
        counter+=1
        print(line,end="")
        if counter == MAX:
            break
    fileIn.close()

for fileName in args.fileList:
    if args.verbose:
        print("\n", fileName, 40*"-")
    headFile(fileName)

exit(0)