# /usr/bin/python
#-*- coding: utf-8-*-
#
# 24-ss-server-one2one.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Gener 2024
# -------------------------------------
import sys, socket, argparse, signal, os
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""Daytime server""")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = ''
PORT = args.port
llistaPeers = []

def llista(signum, frame):
    print("Signal handler called with signal:", signum)
    print(llistaPeers)
    sys.exit(0)


def contador(signum, frame):
    print("Signal handler called with signal:", signum)
    print(len(llistaPeers))
    sys.exit(0)

def completo(signum, frame):
    print("Signal handler called with signal:", signum)
    print(llistaPeers, len(llistaPeers))
    sys.exit(0)

pid = os.fork()
if pid !=0:
   print("Engegat el server SS:", pid)
   sys.exit(0)

signal.signal(signal.SIGUSR1,llista)          #10
signal.signal(signal.SIGUSR2,contador)          #12
signal.signal(signal.SIGTERM,completo)          #15
#-------------------------------------
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
  conn, addr = s.accept()
  print("Connected by", addr)
  llistaPeers.append(addr)
  command = "ss -ltn"
  pipeData = Popen(command,shell=True,stdout=PIPE)
  for line in pipeData.stdout:
    conn.send(line)
  conn.close()

