# /user/bin/python3
#-*- coding: utf-8-*-
# @ Marco3008
# programa.py ruta
# una sola ruta positional argument.
#-----------------------------
import sys, argparse
from subprocess import  Popen, PIPE
parser = argparse.ArgumentParser(description=\
        "Exemple popen")
parser.add_argument("ruta", type=str,\
        help="directori a llistar")
args=parser.parse_args()
#-----------------------------
command = [ "ls", args.ruta ] 
# executar la ordre who del sistema opertaiu, 
#llegirla, genera una serie de info i la procesa el codi python
pipeData = Popen(command, stdout=PIPE)

for line in pipeData.stdout:
    print(line.decode("utf-8"), end="")
exit (0)