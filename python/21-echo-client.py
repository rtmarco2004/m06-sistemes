# ejecuta el ejercicio 16-signal.py

# /usr/bin/python3
#-*- coding: utf-8-*-
#
# -------------------------------------
# @ marco3008 Curs 2023-2024
# exemple-echoclient.py
# -------------------------------------

import sys,socket
HOST = ''
# HOST = 'i23'
# PORT = 7
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s.setcokopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))
s.send(b'hello,world')
data = s.recv(1024)
s.close()
print("Receivded:", repr(data))
sys.exit(0)
