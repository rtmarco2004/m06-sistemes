# /usr/bin/python3
#-*- coding: utf-8-*-
#
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Exemple fork
# -------------------------------------
import sys,os,signal

def pare(signum,frame):
    print("Signal handler called with signal:", signum)
    print("Hola Preciose")


def fill(signum,frame):
    print("Signal handler called with signal:", signum)
    print("Adios feito")
    sys.exit(0)


print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())


pid=os.fork()
if pid !=0:
#    os.wait()
    print("Programa pare: ", os.getpid(), pid)
    print("Llançant el procés fill servidor")
    sys.exit(0)

print("Programa fill: ", os.getpid(), pid)
signal.signal(signal.SIGUSR1,pare)
signal.signal(signal.SIGUSR2,fill)
while True:
    pass

#aixo no s'executara mai
print("Hasta luego Lucas!")
sys.exit(0)