#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: http://docs.python.org/3/library/argparse.html
# argparse tutorial: https://docs.python.org/3/howto/argparse.html
#
# @marco3008 M06 Curs 2023-2024
#--------------------

import argparse

parser = argparse.ArgumentParser(\
    description="programa de exemple d'arguments",\
    prog="02-arguments.py",\
    epilog="Esto es todo esto todo amigos :)")

parser.add_argument("-n","--nom", type=str,\
    help="nom usuari")

parser.add_argument("-e","--edat", type=int,\
    dest="userEdat", help="edat a processar",\
    metavar="edat")
args=parser.parse_args()
print(args)
print(args.userEdat,args.nom)
exit(0)
