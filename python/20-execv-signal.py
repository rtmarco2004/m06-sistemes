# ejecuta el ejercicio 16-signal.py

# /usr/bin/python3
#-*- coding: utf-8-*-
#
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# execv
# -------------------------------------
import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    print("Programa pare: ", os.getpid(), pid)

#programa fill  
#os.execv("/usr/bin/ls", ["/usr/bin/ls", "-ls", "/"])  
#os.execl("/usr/bin/ls", "/usr/bin/ls", "-la", "/")
#os.execlp("ls","ls","-la","/")
#os.execvp("uname",["uname","-a"])
#os.execv("/bin/bash", ["/bin/bash","show.sh"])
os.execv("/usr/bin/python3",[ "/usr/bin/python3", "16b-signal.py", "60"])

# no se ejecuta nunca
print("Hasta luego Lucas!")
sys.exit(0)