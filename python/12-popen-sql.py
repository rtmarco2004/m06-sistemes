# /user/bin/python3
#-*- coding: utf-8-*-
# @ Marco3008
# popen_sql.py
#-----------------------------
import sys, argparse
from subprocess import  Popen, PIPE
parser = argparse.ArgumentParser(description=\
        "Exemple popen")
args=parser.parse_args()
#-----------------------------
command = "psql -qtA -F',' -h localhost -U postgres training -c \"select * from repventas; \""
# executar la ordre who del sistema opertaiu, 
#llegirla, genera una serie de info i la procesa el codi python
pipeData = Popen(command, shell=True, stdout=PIPE)
for line in pipeData.stdout:
    print(line.decode("utf-8"), end="")
exit (0)