# /usr/bin/python3
#-*- coding: utf-8-*-
#
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Exemple fork
# -------------------------------------
import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())


pid=os.fork()
if pid !=0:
#    os.wait()
    print("Programa pare: ", os.getpid(), pid)

else: 
    print("Programa fill: ", os.getpid(), pid)
    while True:
        pass

print("Hasta luego Lucas!")
sys.exit(0)