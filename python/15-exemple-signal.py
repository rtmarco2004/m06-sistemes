# /usr/bin/python
#-*- coding: utf-8-*-
#
# signal.exemple.py
#---------------------------------
import sys, os, signal

def myhandler(signum, frame):
    print("Signal handler with signal: ", signum)
    print("Hasta luego lucas!")
    sys.exit(0)

def myhandler2(signum, frame):
    print("Signal handler with signal:: ", signum)
    print("No me voy a cerrar asquerose >:(")

def sumar(signum, frame):
    print("Signal handler with signal:: ", signum)
    print("Te sumo otros 60 segundos mi rey")
    signal.alarm(+60)

def restar(signum, frame):
    print("Signal handler with signal:: ", signum)
    print("Te resto 60 segundos por tontito D:")    
    signal.alarm(-60)


# Assignar un handler al senyal
signal.signal(signal.SIGUSR1,myhandler) #10 es el senyal de SIGUSR1; handler --> manipulador
signal.signal(signal.SIGUSR2,myhandler2) #12 es el senyal de SIGUSR2; handler --> manipulador
signal.signal(signal.SIGALRM,myhandler) #14 es el senyal de SIGALARM; handler --> manipulador
signal.alarm(60)

print(os.getpid()) # per saber el pid del proces
while True: # fa un bucle infinit
    pass
sys.exit(0)


