# /usr/bin/python3
#-*- coding: utf-8-*-
# 26-telnet-client-one2one.py  
# -------------------------------------
# @marco3008
# Maig 2024
# -------------------------------------
import sys,socket,argparse,os
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="telnet client")
parser.add_argument("-p", "--port",type=int, \
        dest="port",\
        metavar="port")
parser.add_argument("-s","--server", type=str,\
        dest="server",\
        metavar="server")
args=parser.parse_args()
HOST = args.server
PORT = args.port
ACABAR = bytes(chr(4),'utf-8')
# -------------------------------------------------
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
s.connect((HOST,PORT))
while True:
    ordre = input("$ ")
    if not ordre: break
    s.sendall(bytes(ordre,'utf-8'))
    while True:
        data = s.recv(1024)
        if data[-1:] == ACABAR:
            print(str(data[:-1]))
            break
        print(str(data))
s.close()
sys.exit(0)
