# ejecuta el ejercicio 16-signal.py

# /usr/bin/python3
#-*- coding: utf-8-*-
#
# -------------------------------------
# @ marco3008 Curs 2023-2024
# exemple-echoserver.py
# -------------------------------------

import sys,socket

HOST = ''  # es com posar per totes les interficies dell propi host (0.0.0.0)
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # crea un objecte de tipus socket. 
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # Quan es tanca el prog, el port torna a estar live, si no, el port triga en tornar a esta disp
s.bind((HOST,PORT)) # escolata per el port 50001 i per localhost
s.listen(1) # el 1 no pinta res, s.listen es posa a escoltar.
conn, addr = s.accept() # aqui es queda clavat a esperar a acceptar una conexio client.
print("Conn:", type(conn), conn)
print("Connected by:,addr")
while True:
    data = conn.recv(1024) # 1024 s'ignora
    if not data: break # if not dada, no s'ignifica no hi ha dades, vol dir si han colgat la connexio.
    conn.send(data)
    print(data)
conn.close()
sys.exit(0)


#AF_INET --> Estem treballant amb protocol de xarxa d'internet. 
#SOCK_STREAM --> tcp
# s.accept --> retorna una tupla 
# conn -> es un objecte creat per una connexio, es a dir, l'objecte es crea quan un client 