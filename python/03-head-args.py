#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n nlin] [-f file]
# default=10, file o stdin
# @marco3008 M06 Curs 2023-2024
#--------------------

import sys
import argparse

parser = argparse.ArgumentParser(\
    description="mostra les n linies del argument",\
    prog="03-arguments.py",\
    epilog="Esto es todo esto todo amigos :)")

parser.add_argument("-n","--nlin",dest="nlin", default=5, type=int,\
    help="numero de linies a mostrar", metavar="NumeroLinies")

parser.add_argument("-f","--fit", type=str,\
    dest="fitxer", help="fitxer a processar", default="/dev/stdin",\
    metavar="file")
args=parser.parse_args()
print(args)

MAX=args.nlin
fileIn=open(args.fitxer,"r")
counter=0

for line in fileIn:
    counter+=1
    print(line,end="")
    if counter == MAX:
        break

fileIn.close()
exit(0)