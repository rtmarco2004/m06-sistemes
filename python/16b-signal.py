# /usr/bin/python3
#-*- coding: utf-8-*-
#
# signal.py segons
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Abril 2024
# -------------------------------------
import sys, os, signal, argparse
parser = argparse.ArgumentParser(description="Gestinar alarma")
parser.add_argument("segons", type=int, help="segons")
args=parser.parse_args()
print(args)
# -------------------------------------
up=0
down=0

def myusr1(signum, frame):
  print("Signal handler with signal: ", signum)

def myusr2(signum, frame):
  print("Signal handler with signal:", signum)

def myalarm(signum,frame):
  print("Signal handler called with signal:", signum)
  print("Finalitzant... up: %d down:%d restant: %d" % (up, down, signal.alarm(0)))
  sys.exit(0)

def myterm(signum,frame):   
  print("Signal handler called with signal:", signum)

def myhup(signum,frame):
  print("Signal handler called with signal:", signum)


# Assignar un handler al senyal
signal.signal(signal.SIGUSR1,myusr1)          #10
signal.signal(signal.SIGUSR2,myusr2)          #12
signal.signal(signal.SIGALRM,myalarm)         #14
signal.signal(signal.SIGTERM,myterm)          #15
signal.signal(signal.SIGHUP,myhup)            #1
signal.signal(signal.SIGINT,signal.SIG_IGN)   #2

signal.alarm(args.segons)
print(os.getpid())
while True:
  pass    
sys.exit(0)



