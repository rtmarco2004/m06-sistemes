#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n 5 o 10 o 15] file
# default=10, file o stdin
# Un argument obligatori que ha de ser file
# @marco3008 M06 Curs 2023-2024
#--------------------

import sys
import argparse

parser = argparse.ArgumentParser(\
    description="mostra les n linies del argument",\
    prog="03-arguments.py",\
    epilog="Esto es todo esto todo amigos :)")

parser.add_argument("-n","--nlin",dest="nlin", default=5, type=int,\
    help="numero de linies a mostrar", metavar="NumeroLinies" , choices=[5,10,15])

parser.add_argument("fitxer", type=str,\
    help="fitxer a processar", metavar="file")
args=parser.parse_args()
print(args)

MAX=args.nlin
fileIn=open(args.fitxer,"r")
counter=0

for line in fileIn:
    counter+=1
    print(line,end="")
    if counter == MAX:
        break

fileIn.close()
exit(0)