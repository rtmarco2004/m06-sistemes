# ejecuta el ejercicio 16-signal.py

# /usr/bin/python3
#-*- coding: utf-8-*-
#
# -------------------------------------
# @ marco3008 Curs 2023-2024
# daytime-client.py
# -------------------------------------

import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
    data = s.recv(1024)
    if not data: break
    print("Data:", repr(data))
s.close()
sys.exit(0)