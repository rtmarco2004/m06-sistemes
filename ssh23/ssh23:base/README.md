# SSH23 BASE

# Dockerfile:

```
# ldapserver
FROM debian:12
LABEL version="1.0"
LABEL author="marco"
RUN apt-get update
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get  -y install procps iproute2 tree nmap vim less finger passwd  libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils libpam-mount libpam-pwquality openssh-server openssh-client net-tools
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
``` 

# Proves:

## Comprovem que tenim el port propagat:

```
linuxmarco@Marco:~/m06-sistemes/ssh23/ssh23:base$ docker ps
CONTAINER ID   IMAGE                     COMMAND                  CREATED              STATUS              PORTS                                   NAMES
f3088d661316   marco3008/phpadmin        "/bin/sh -c /opt/doc…"   About a minute ago   Up 3 seconds        0.0.0.0:80->80/tcp, :::80->80/tcp       phpldapadmin.edt.org
93952b530446   marco3008/ldap23:latest   "/bin/sh -c /opt/doc…"   About a minute ago   Up About a minute   389/tcp                                 ldap.edt.org
fbd90766dbdd   marco3008/ssh23:base      "/bin/sh -c /opt/doc…"   About a minute ago   Up About a minute   0.0.0.0:2022->22/tcp, :::2022->22/tcp   ssh.edt.org
```

## Accedir als usuaris unix:

```
unix01@ssh:~$ su - unix03
Password: 
unix03@ssh:~$ 
```

## Accedir als usuaris ldap:

```
unix03@ssh:~$ su - pere
Password: 
Creating directory '/tmp/home/pere'.
$ pwd
/tmp/home/pere
$ id
uid=5001(pere) gid=601(professors) groups=601(professors)
```

# Proves per ssh:

Intentem entrar al usuari pere, la primera vegada que entrem ens demanarà el fingerrint, pero la segona no ho demana ja que s'ha guardat a .ssh/known_hosts.

```
linuxmarco@Marco:~/m06-sistemes/ssh23/ssh23:base$ ssh pere@172.20.0.2
The authenticity of host '172.20.0.2 (172.20.0.2)' can't be established.
ECDSA key fingerprint is SHA256:NQE1vyQLw01L2vQg+kA0zeXT4wgq4xREHffyrbNt0G8.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.20.0.2' (ECDSA) to the list of known hosts.
pere@172.20.0.2's password: 
Linux ssh.edt.org 5.10.0-18-amd64 #1 SMP Debian 5.10.140-1 (2022-09-02) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
$
```

# Exemple 1:

Entrar per ssh a unix01 mostrant el visual hostkey i donant permis per introduir la passwd.

```
root@ssh:/opt/docker# ssh -o VisualHostKey=yes -o PasswordAuthentication=yes unix01@localhost
Host key fingerprint is SHA256:W6CpizGdFyaAj4B0yWV25w2dUToTzG/2YrEvvpO9MwA
+--[ED25519 256]--+
| ...o+ . o+o+.   |
|o..oo . o o=o    |
|+ .     .. =.    |
|.o .   o . Eo=   |
|. . . = S . + +  |
|   . = . o   = . |
|  o + . .   . *  |
|   + o       + = |
|  . .       .o+.+|
+----[SHA256]-----+
unix01@localhost's password: 
Linux ssh.edt.org 5.10.0-18-amd64 #1 SMP Debian 5.10.140-1 (2022-09-02) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
unix01@ssh:~$ 
```

# Exemple 2:

Mateix exemple que el primer pero en aquest cas editem el fitxer /etc/ssh/ssh_config 

```
PasswordAuthentication=yes
VisualHostKey=yes
```

```
root@ssh:/opt/docker# ssh unix01@localhost
Host key fingerprint is SHA256:W6CpizGdFyaAj4B0yWV25w2dUToTzG/2YrEvvpO9MwA
+--[ED25519 256]--+
| ...o+ . o+o+.   |
|o..oo . o o=o    |
|+ .     .. =.    |
|.o .   o . Eo=   |
|. . . = S . + +  |
|   . = . o   = . |
|  o + . .   . *  |
|   + o       + = |
|  . .       .o+.+|
+----[SHA256]-----+
unix01@localhost's password: 
Linux ssh.edt.org 5.10.0-18-amd64 #1 SMP Debian 5.10.140-1 (2022-09-02) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Wed Feb  7 22:37:03 2024 from 127.0.0.1
unix01@ssh:~$ 
```

# Config acces per ssh destaes via clau publica:

En aquest apartat crearem un parell de claus amb l'ordre ssh-keygen, de tipus RSA, el directori on les crearem sera a .ssh, el nostre usuari, en aquest cas marco el que vol es tenir la capacitat de poder entrar
automàticament al servidor sense posar la seva clau, per fer aixo utilitzarem l'ordre ssh-copy-id.

### Generar parell de claus:

```
ssh-keygen
```

```
linuxmarco@Marco:~/.ssh$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/linuxmarco/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/linuxmarco/.ssh/id_rsa
Your public key has been saved in /home/linuxmarco/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:CTcCbrqspoxBzQn4kIkSfW8m3Oj5DTw+fjZzUX5rIO0 linuxmarco@Marco
The key's randomart image is:
+---[RSA 3072]----+
|..  .            |
|o+....           |
|B. oo+o o        |
|.o+o= =+ o  .    |
| .o= *  S  +     |
|.. .o +   o + .  |
|. o  o +   + o . |
|o+    + * . E o  |
|*.   ..+ +   .   |
+----[SHA256]-----+
```

```
linuxmarco@Marco:~/.ssh$ ls -l
total 12
-rw------- 1 linuxmarco linuxmarco 2602 feb  7 23:47 id_rsa
-rw-r--r-- 1 linuxmarco linuxmarco  570 feb  7 23:47 id_rsa.pub
-rw-r--r-- 1 linuxmarco linuxmarco 3552 feb  7 23:34 known_hosts
```

Com podem veure les claus s'han generat i hara les copiem dins del usuari on ens volem conectar

```
ssh-copy-id -i ~/.ssh/id_rsa.pub marta@172.20.0.2
```

```
linuxmarco@Marco:~/.ssh$ ssh-copy-id -i ~/.ssh/id_rsa.pub marta@172.20.0.2
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/linuxmarco/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
marta@172.20.0.2's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'marta@172.20.0.2'"
and check to make sure that only the key(s) you wanted were added.
```

Per comprovar que ha funcionat nosaltres entrem dins del usuari marta i comprovem si tenim un fitxer anomenat authorized_keys

```
marta@ssh:~/.ssh$ ls -la
total 12
drwx------ 2 marta alumnes 4096 Feb  7 22:50 .
drwxr-xr-x 3 marta alumnes 4096 Feb  7 22:50 ..
-rw------- 1 marta alumnes  570 Feb  7 22:50 authorized_keys
```

```
marta@ssh:~/.ssh$ cat authorized_keys 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCj0bny2xzZUvl9oXL0oH3dlEwEMX2sEyMiYbABOIHdZYFfOYg3Xdv7i2yzMzKaY/Ixo7wBirSYvU/kkN4dH3svidqS6yTvXtRfaPMdsf0C+sy0q6cqM59e7g58DWZRkVkJabsNzvdP/4nt6yeLxfE0KpdrKEQEKLuhPWZFDKlGACI7ANY+4vgPB8HXrkGGhcciC7yTPbNursWZGkAfTG8SvSCU137PQkvanZnEcbnNweesS1hb8fpM2LwQ4sEHYrYMv+p4PEI0WapfLgiaflrrGf/UBwjm27GK+IeZpMQabVefVmTfRwi3IDuew+Z9GJN+Y7dIOr2w8WmDnMqPfBLS8uZdE4Pz+nbF3gfhVd6cEO4Jz//QkAE42Ewd3suQW9hNd7zoAd0OZvi/AUqEWKBifofb0jP93LLnCCOG5N2QEFmZbOjqvHO8HOo+WMlm5L1qbXgsLmgGSItdAjcKARgeoRBcvhhRVrQbxXQL3fSgS33XTiqK5ObugXq25T/hPu0= linuxmarco@Marco
```

Si hara nosaltres intentem entrar per ssh veurem que no ens demana cap constrasenya

```
linuxmarco@Marco:~/.ssh$ ssh marta@172.20.0.2
Linux ssh.edt.org 5.10.0-18-amd64 #1 SMP Debian 5.10.140-1 (2022-09-02) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
$ 
```
